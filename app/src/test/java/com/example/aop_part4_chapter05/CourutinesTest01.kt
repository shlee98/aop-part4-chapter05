package com.example.aop_part4_chapter05

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.system.measureTimeMillis

class CourutinesTest01 {

    @Test
    fun test01() = runBlocking {

        val time = measureTimeMillis {
            val firstName = getFirst()
            val lastName = getLast()
            print("Hello, $firstName $lastName")
        }
        print("\nmeause time: $time\n")
    }

    @Test
    fun test02() = runBlocking {
        val time = measureTimeMillis {
            val firstName = async { getFirst() }
            val lastName = async { getLast() }
            print("Hello, ${firstName.await()} ${lastName.await()}")
        }
        print("\nmeause time: $time\n")
    }

    suspend fun getFirst(): String {
        delay(1000)
        return "이"
    }

    suspend fun getLast(): String {
        delay(1000)
        return "성현"
    }
}